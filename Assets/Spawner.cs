using UnityEngine;

public class Spawner : MonoBehaviour
{
    public GameObject asteroid;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Random.Range(0,100) < 1)
        {
            //Instantiate(asteroid, transform.position + new Vector3(Random.Range(-15, 15), 0, 0), Quaternion.identity);
            GameObject a = Pool.singleton.Get("asteroid");
            if (a != null)
            {
                a.transform.position = transform.position +
                    new Vector3(Random.Range(-10, 10), 0, 0);
                a.SetActive(true);
            }
        }

    }
}
